FileSearchTool 文件搜索工具

#### 项目简介：
FileSearchTool是使用javafx开发的一款文件搜索工具，使用[lucene](https://lucene.apache.org/) 搜索引擎打造高效快速多条件的文件搜索工具。

#### 功能特色
1. 支持正则表达式规则过滤文件
2. 支持隐藏文件、文件大小过滤、文件名模糊搜索等条件快速定位文件

**xJavaFxTool交流QQ群：== [387473650](https://jq.qq.com/?_wv=1027&k=59UDEAD) ==**

#### 环境搭建说明：
- 开发环境为jdk1.8，基于maven构建
- 使用eclipase或Intellij Idea开发(推荐使用[Intellij Idea](https://www.jetbrains.com/?from=xJavaFxTool))
- 该项目为javaFx开发的实用小工具集[xJavaFxTool](https://gitee.com/xwintop/xJavaFxTool)的插件。
- 本项目使用了[lombok](https://projectlombok.org/),在查看本项目时如果您没有下载lombok 插件，请先安装,不然找不到get/set等方法
- 依赖的[xcore包](https://gitee.com/xwintop/xcore)已上传至git托管的maven平台，git托管maven可参考教程(若无法下载请拉取项目自行编译)。[教程地址：点击进入](http://blog.csdn.net/u011747754/article/details/78574026)

![文件搜索工具.png](images/文件搜索工具.png)